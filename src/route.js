import React,  {Component} from 'react';
import { BrowserRouter, Switch, Route} from "react-router-dom";

import  User  from './Container/User/User';
import  Post  from './Container/Post/Post';
import  Comment  from './Container/Comment/Comment';


class Routes extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route exact path='/' component={User} />
             <Route exact path='/post/:id' component={Post} />
              <Route exact path='/comment/:id' component={Comment} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}


export default Routes;
